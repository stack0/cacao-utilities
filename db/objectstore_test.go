// Package db contains utility functions for dealing with database
package db

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
)

type testobject struct {
	id    string `bson:"_id" json:"id,omitempty"`
	owner string `bson:"owner" json:"owner,omitempty"`
	field string `bson:"field" json:"name,omitempty"`
}

func TestCreateObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)
}

func TestReleaseObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	err = store.Release()
	assert.NoError(t, err)
	assert.Nil(t, store.Mock)
}

func TestListObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	expectedResults := []testobject{
		{
			id:    "0001",
			owner: "test_owner1",
			field: "test_field1",
		},
		{
			id:    "0002",
			owner: "test_owner2",
			field: "test_field2",
		},
	}
	store.Mock.On("List", "test_collection").Return(expectedResults, nil)

	results := []testobject{}
	err = store.List("test_collection", &results)
	assert.NoError(t, err)
	assert.ElementsMatch(t, results, expectedResults)

	err = store.Release()
	assert.NoError(t, err)
}

func TestListConditionalObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	expectedResults := []testobject{
		{
			id:    "0001",
			owner: "test_owner1",
			field: "test_field1",
		},
		{
			id:    "0002",
			owner: "test_owner2",
			field: "test_field2",
		},
	}
	store.Mock.On("ListConditional", "test_collection", map[string]interface{}{}).Return(expectedResults, nil)

	results := []testobject{}
	err = store.ListConditional("test_collection", bson.M{}, &results)
	assert.NoError(t, err)
	assert.ElementsMatch(t, results, expectedResults)

	err = store.Release()
	assert.NoError(t, err)
}

func TestListConditionalObjectStore2(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	expectedResults := []testobject{
		{
			id:    "0001",
			owner: "test_owner1",
			field: "test_field1",
		},
	}
	store.Mock.On("ListConditional", "test_collection", map[string]interface{}{"id": "0001"}).Return(expectedResults, nil)

	results := []testobject{}
	err = store.ListConditional("test_collection", bson.M{"id": "0001"}, &results)
	assert.NoError(t, err)
	assert.ElementsMatch(t, results, expectedResults)

	err = store.Release()
	assert.NoError(t, err)
}

func TestListForUserObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	expectedResults := []testobject{
		{
			id:    "0001",
			owner: "test_owner1",
			field: "test_field1",
		},
		{
			id:    "0002",
			owner: "test_owner1",
			field: "test_field2",
		},
	}
	store.Mock.On("ListForUser", "test_collection", "test_owner1").Return(expectedResults, nil)

	results := []testobject{}
	err = store.ListForUser("test_collection", "test_owner1", &results)
	assert.NoError(t, err)
	assert.ElementsMatch(t, results, expectedResults)

	err = store.Release()
	assert.NoError(t, err)
}

func TestGetConditionalObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	expectedResult := testobject{
		id:    "0001",
		owner: "test_owner1",
		field: "test_field1",
	}
	store.Mock.On("GetConditional", "test_collection", map[string]interface{}{"id": "0001"}).Return(expectedResult, nil)

	result := testobject{}
	err = store.GetConditional("test_collection", bson.M{"id": "0001"}, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, &result)
	assert.Equal(t, result.owner, "test_owner1")

	err = store.Release()
	assert.NoError(t, err)
}

func TestGetObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	expectedResult := testobject{
		id:    "0001",
		owner: "test_owner1",
		field: "test_field1",
	}
	store.Mock.On("Get", "test_collection", "0001").Return(expectedResult, nil)

	result := testobject{}
	err = store.Get("test_collection", "0001", &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, &result)
	assert.Equal(t, result.owner, "test_owner1")

	err = store.Release()
	assert.NoError(t, err)
}

func TestInsertObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	insertObject := testobject{
		id:    "0001",
		owner: "test_owner1",
		field: "test_field1",
	}
	store.Mock.On("Insert", "test_collection").Return(nil)

	err = store.Insert("test_collection", insertObject)
	assert.NoError(t, err)

	err = store.Release()
	assert.NoError(t, err)
}

func TestUpdateObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	updateObject := testobject{
		id:    "0001",
		owner: "test_owner1",
		field: "test_field1",
	}
	store.Mock.On("Update", "test_collection", "0001").Return(true, nil)

	result, err := store.Update("test_collection", "0001", updateObject)
	assert.NoError(t, err)
	assert.True(t, result)

	err = store.Release()
	assert.NoError(t, err)
}

func TestReplaceObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	replaceObject := testobject{
		id:    "0001",
		owner: "test_owner1",
		field: "test_field1_updated",
	}
	store.Mock.On("Replace", "test_collection", "0001").Return(true, nil)

	result, err := store.Replace("test_collection", "0001", replaceObject)
	assert.NoError(t, err)
	assert.True(t, result)

	err = store.Release()
	assert.NoError(t, err)
}

func TestDeleteObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	store.Mock.On("Delete", "test_collection", "0001").Return(true, nil)

	result, err := store.Delete("test_collection", "0001")
	assert.NoError(t, err)
	assert.True(t, result)

	err = store.Release()
	assert.NoError(t, err)
}

func TestDeleteConditionalObjectStore(t *testing.T) {
	store, err := CreateMockObjectStore()
	assert.NoError(t, err)
	assert.NotEmpty(t, store)
	assert.NotNil(t, store.Mock)

	store.Mock.On("DeleteConditional", "test_collection", map[string]interface{}{"id": "0001"}).Return(1, nil)

	result, err := store.DeleteConditional("test_collection", bson.M{"id": "0001"})
	assert.NoError(t, err)
	assert.Equal(t, result, int64(1))

	err = store.Release()
	assert.NoError(t, err)
}
