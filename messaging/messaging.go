// Package messaging contains utility functions for dealing with a massaging service
package messaging

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	cacao_types_service "gitlab.com/cyverse/cacao-types/service"
)

// QueryEventHandler is a function prototype for query event handlers
type QueryEventHandler func(subject cacao_types_service.QueryQueue, jsonData []byte) ([]byte, error)

// QueryCloudEventHandler is a function prototype for query event handlers
type QueryCloudEventHandler func(event *cloudevents.Event) ([]byte, error)

// StreamingEventHandler is a function prototype for streaming event handlers
type StreamingEventHandler func(subject cacao_types_service.EventType, jsonData []byte) error

// StreamingCloudEventHandler is a function prototype for streaming event handlers
type StreamingCloudEventHandler func(event *cloudevents.Event) error

// QueryEventService is an interface for a query event service (e.g., Nats)
type QueryEventService interface {
	Disconnect() error
	AddEventHandler(subject cacao_types_service.QueryQueue, eventHandler QueryEventHandler) error
	AddCloudEventHandler(subject cacao_types_service.QueryQueue, eventHandler QueryCloudEventHandler) error
	Request(subject cacao_types_service.QueryQueue, data interface{}) ([]byte, error)
	RequestCloudEvent(ce *cloudevents.Event) ([]byte, error)
}

// StreamingEventService is an interface for a streaming event service (e.g., Stan)
type StreamingEventService interface {
	Disconnect() error
	AddEventHandler(subject cacao_types_service.EventType, eventHandler StreamingEventHandler) error
	AddCloudEventHandler(subject cacao_types_service.EventType, eventHandler StreamingCloudEventHandler) error
	Publish(subject cacao_types_service.EventType, data interface{}) error
	PublishCloudEvent(ce *cloudevents.Event) error
}
