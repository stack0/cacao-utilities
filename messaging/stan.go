// Package messaging contains utility functions for dealing with a massaging service
package messaging

import (
	"fmt"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	stan "github.com/nats-io/stan.go"
	log "github.com/sirupsen/logrus"
	cacao_types_config "gitlab.com/cyverse/cacao-types/config"
	cacao_types_service "gitlab.com/cyverse/cacao-types/service"
	cloudevent "gitlab.com/cyverse/cacao-utilities/cloudevent"
)

// StanConnection contains Stan connection info
type StanConnection struct {
	NatsConfig         *cacao_types_config.NatsConfig
	StanConfig         *cacao_types_config.StanConfig
	Connection         stan.Conn
	EventHandlers      map[cacao_types_service.EventType]StreamingEventHandler
	CloudEventHandlers map[cacao_types_service.EventType]StreamingCloudEventHandler
}

// ConnectStan connects to Stan
func ConnectStan(natsConfig *cacao_types_config.NatsConfig, stanConfig *cacao_types_config.StanConfig) (*StanConnection, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-utilities.nats",
		"function": "ConnectStan",
	})

	logger.Tracef("trying to connect to %s", natsConfig.URL)

	// Connect to Stan
	sc, err := stan.Connect(stanConfig.ClusterID, natsConfig.ClientID, stan.NatsURL(natsConfig.URL))
	if err != nil {
		logger.WithError(err).Errorf("failed to connect to %s", natsConfig.URL)
		return nil, err
	}

	stanConn := &StanConnection{
		NatsConfig:         natsConfig,
		StanConfig:         stanConfig,
		Connection:         sc,
		EventHandlers:      map[cacao_types_service.EventType]StreamingEventHandler{},
		CloudEventHandlers: map[cacao_types_service.EventType]StreamingCloudEventHandler{},
	}

	// subscribe a common cyverse event channel
	handler := func(msg *stan.Msg) {
		err := msg.Ack()
		if err != nil {
			logger.Error(err)
		}

		ce, err := cloudevent.ConvertStan(msg)
		if err != nil {
			logger.Error(err)
		} else {
			// handle events
			err := stanConn.callEventHandler(&ce)
			if err != nil {
				logger.Error(err)
			}
		}
	}

	_, err = sc.QueueSubscribe(string(cacao_types_service.StreamingEventSubject), natsConfig.QueueGroup, handler, stan.DurableName(stanConfig.DurableName))
	if err != nil {
		logger.Error(err)
		sc.Close()
		return nil, err
	}

	logger.Tracef("established a connection to %s", natsConfig.URL)

	return stanConn, nil
}

// ConnectStanForSender connects to Stan for sending events
func ConnectStanForSender(natsConfig *cacao_types_config.NatsConfig, stanConfig *cacao_types_config.StanConfig) (*StanConnection, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-utilities.nats",
		"function": "ConnectStanForSender",
	})

	logger.Tracef("trying to connect to %s", natsConfig.URL)

	// Connect to Stan
	sc, err := stan.Connect(stanConfig.ClusterID, natsConfig.ClientID, stan.NatsURL(natsConfig.URL))
	if err != nil {
		logger.WithError(err).Errorf("failed to connect to %s", natsConfig.URL)
		return nil, err
	}

	stanConn := &StanConnection{
		NatsConfig:         natsConfig,
		StanConfig:         stanConfig,
		Connection:         sc,
		EventHandlers:      map[cacao_types_service.EventType]StreamingEventHandler{},
		CloudEventHandlers: map[cacao_types_service.EventType]StreamingCloudEventHandler{},
	}

	logger.Tracef("established a connection to %s", natsConfig.URL)

	return stanConn, nil
}

// Disconnect disconnects Stan connection
func (conn *StanConnection) Disconnect() error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-utilities.nats",
		"function": "StanConnection.Disconnect",
	})

	logger.Tracef("trying to disconnect from %s", conn.NatsConfig.URL)

	conn.Connection.Close()

	logger.Tracef("disconnected from %s", conn.NatsConfig.URL)

	// clear
	conn.Connection = nil
	conn.EventHandlers = map[cacao_types_service.EventType]StreamingEventHandler{}
	conn.CloudEventHandlers = map[cacao_types_service.EventType]StreamingCloudEventHandler{}
	return nil
}

// AddEventHandler adds a new event handler function to a specified subject
// eventHandler receives subject and JSON data of an event
func (conn *StanConnection) AddEventHandler(subject cacao_types_service.EventType, eventHandler StreamingEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-utilities.nats",
		"function": "StanConnection.AddEventHandler",
	})

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.EventHandlers[cacao_types_service.EventType(subject)] = eventHandler

	logger.Tracef("added an event handler for a subject %s", subject)

	return nil
}

// AddCloudEventHandler adds a new event handler function to a specified subject
// eventHandler receives a cloudevent of an event
func (conn *StanConnection) AddCloudEventHandler(subject cacao_types_service.EventType, eventHandler StreamingCloudEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-utilities.nats",
		"function": "StanConnection.AddCloudEventHandler",
	})

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.CloudEventHandlers[cacao_types_service.EventType(subject)] = eventHandler

	logger.Tracef("added an event handler for a subject %s", subject)

	return nil
}

// Publish publishes Stan event
func (conn *StanConnection) Publish(subject cacao_types_service.EventType, data interface{}) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-utilities.nats",
		"function": "StanConnection.Publish",
	})

	logger.Tracef("publishing a subject %s", subject)

	ce, err := cloudevent.CreateCloudEvent(data, string(subject), conn.NatsConfig.ClientID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event for subject %s", subject)
		return err
	}

	eventJSON, err := ce.MarshalJSON()
	if err != nil {
		logger.WithError(err).Errorf("failed to marshal an event to JSON")
		return err
	}

	err = conn.Connection.Publish(cacao_types_service.StreamingEventSubject, eventJSON)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish a subject %s", ce.Type())
		return err
	}

	logger.Tracef("published a subject %s", ce.Type())

	return nil
}

// PublishCloudEvent publishes Stan event
func (conn *StanConnection) PublishCloudEvent(ce *cloudevents.Event) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-utilities.nats",
		"function": "StanConnection.PublishCloudEvent",
	})

	// we put a subject into Event.Type instead of Event.Subject
	// please refer 'CreateCloudEvent' function in cloudevent.go
	logger.Tracef("publishing a subject %s", ce.Type())

	eventJSON, err := ce.MarshalJSON()
	if err != nil {
		logger.WithError(err).Errorf("failed to marshal an event to JSON")
		return err
	}

	err = conn.Connection.Publish(cacao_types_service.StreamingEventSubject, eventJSON)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish a subject %s", ce.Type())
		return err
	}

	logger.Tracef("published a subject %s", ce.Type())

	return nil
}

// calls a event handler for an event
func (conn *StanConnection) callEventHandler(event *cloudevents.Event) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-utilities.nats",
		"function": "StanConnection.callEventHandler",
	})

	// we put a subject into Event.Type instead of Event.Subject
	// please refer 'CreateCloudEvent' function in cloudevent.go
	logger.Tracef("handling an event %s", event.Type())

	if handler, ok := conn.EventHandlers[cacao_types_service.EventType(event.Type())]; ok {
		// has the handler for the event
		err := handler(cacao_types_service.EventType(event.Type()), event.Data())
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return err
		}
		return nil
	} else if handler, ok := conn.CloudEventHandlers[cacao_types_service.EventType(event.Type())]; ok {
		// has the handler for the event
		err := handler(event)
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return err
		}
		return nil
	} else {
		// ignore events
		return nil
	}
}
