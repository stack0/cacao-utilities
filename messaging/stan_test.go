// Package messaging contains utility functions for dealing with a massaging service
package messaging

import (
	"encoding/json"
	"testing"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/stretchr/testify/assert"

	cacao_types_config "gitlab.com/cyverse/cacao-types/config"
	cacao_types_service "gitlab.com/cyverse/cacao-types/service"
	cloudevent "gitlab.com/cyverse/cacao-utilities/cloudevent"
)

type testStanMsgObject struct {
	ID    string
	Field string
}

var testStanConfig cacao_types_config.StanConfig = cacao_types_config.StanConfig{
	ClusterID:   "test_custer_id",
	DurableName: "test_durable_name",
}

func TestCreateStanConnection(t *testing.T) {
	conn, err := CreateMockStanConnection(&testNatsConfig, &testStanConfig)
	assert.NoError(t, err)
	assert.NotEmpty(t, conn)
}

func TestDisconnectStanConnection(t *testing.T) {
	conn, err := CreateMockStanConnection(&testNatsConfig, &testStanConfig)
	assert.NoError(t, err)
	assert.NotEmpty(t, conn)

	err = conn.Disconnect()
	assert.NoError(t, err)
}

func TestStanSubscribeAndPublish(t *testing.T) {
	conn, err := CreateMockStanConnection(&testNatsConfig, &testStanConfig)
	assert.NoError(t, err)
	assert.NotEmpty(t, conn)

	testSubject := "test_subject"
	testMessage := testStanMsgObject{
		ID:    "0001",
		Field: "test_field1",
	}

	eventHandler := func(subject cacao_types_service.EventType, jsonData []byte) error {
		assert.Equal(t, testSubject, string(subject))

		var receivedMessage testStanMsgObject
		err := json.Unmarshal(jsonData, &receivedMessage)
		assert.NoError(t, err)

		assert.Equal(t, testMessage, receivedMessage)
		return nil
	}

	err = conn.AddEventHandler(cacao_types_service.EventType(testSubject), eventHandler)
	assert.NoError(t, err)

	err = conn.Publish(cacao_types_service.EventType(testSubject), testMessage)
	assert.NoError(t, err)

	err = conn.Disconnect()
	assert.NoError(t, err)
}

func TestStanSubscribeAndRequestCloudEvent(t *testing.T) {
	conn, err := CreateMockStanConnection(&testNatsConfig, &testStanConfig)
	assert.NoError(t, err)
	assert.NotEmpty(t, conn)

	testSubject := "test_subject"
	testMessage := testStanMsgObject{
		ID:    "0001",
		Field: "test_field1",
	}
	testClientID := "test_client_id"

	ce, err := cloudevent.CreateCloudEvent(testMessage, string(testSubject), testClientID)
	assert.NoError(t, err)
	assert.NotNil(t, ce)

	eventHandler := func(event *cloudevents.Event) error {
		assert.Equal(t, testSubject, event.Type())
		assert.Equal(t, testClientID, event.Source())

		var receivedMessage testStanMsgObject
		err := json.Unmarshal(event.Data(), &receivedMessage)
		assert.NoError(t, err)

		assert.Equal(t, testMessage, receivedMessage)

		return nil
	}

	err = conn.AddCloudEventHandler(cacao_types_service.EventType(testSubject), eventHandler)
	assert.NoError(t, err)

	err = conn.PublishCloudEvent(&ce)
	assert.NoError(t, err)

	err = conn.Disconnect()
	assert.NoError(t, err)
}
