// Package messaging contains utility functions for dealing with a massaging service
package messaging

import (
	"fmt"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	nats "github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	cacao_types_config "gitlab.com/cyverse/cacao-types/config"
	cacao_types_service "gitlab.com/cyverse/cacao-types/service"
	cloudevent "gitlab.com/cyverse/cacao-utilities/cloudevent"
)

// NatsConnection contains Nats connection info
type NatsConnection struct {
	Config             *cacao_types_config.NatsConfig
	Connection         *nats.Conn
	EventHandlers      map[cacao_types_service.QueryQueue]QueryEventHandler
	CloudEventHandlers map[cacao_types_service.QueryQueue]QueryCloudEventHandler
}

// ConnectNats connects to Nats
func ConnectNats(config *cacao_types_config.NatsConfig) (*NatsConnection, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-utilities.nats",
		"function": "ConnectNats",
	})

	logger.Tracef("trying to connect to %s", config.URL)

	options := []nats.Option{}

	if config.MaxReconnects >= 0 {
		options = append(options, nats.MaxReconnects(config.MaxReconnects))
	}

	if config.ReconnectWait >= 0 {
		reconnectWait := time.Duration(config.ReconnectWait) * time.Second
		options = append(options, nats.ReconnectWait(reconnectWait))
	}

	// Connect to Nats
	nc, err := nats.Connect(config.URL, options...)
	if err != nil {
		logger.WithError(err).Errorf("failed to connect to %s", config.URL)
		return nil, err
	}

	natsConn := &NatsConnection{
		Config:             config,
		Connection:         nc,
		EventHandlers:      map[cacao_types_service.QueryQueue]QueryEventHandler{},
		CloudEventHandlers: map[cacao_types_service.QueryQueue]QueryCloudEventHandler{},
	}

	if len(config.WildcardSubject) == 0 {
		err := fmt.Errorf("failed to subscribe an empty subject")
		logger.Error(err)
		nc.Close()
		return nil, err
	}

	// Add a handler
	handler := func(msg *nats.Msg) {
		ce, err := cloudevent.ConvertNats(msg)
		if err != nil {
			logger.Error(err)
		} else {
			// handle events
			response, err := natsConn.callEventHandler(&ce)
			if err != nil {
				logger.Error(err)
			} else {
				err = msg.Respond(response)
				if err != nil {
					logger.Error(err)
				}
			}
		}
	}

	// subject should contain wildcards ("*" or ">")
	_, err = nc.QueueSubscribe(config.WildcardSubject, config.QueueGroup, handler)
	if err != nil {
		logger.Error(err)
		nc.Close()
		return nil, err
	}

	logger.Tracef("established a connection to %s", config.URL)

	return natsConn, nil
}

// ConnectNatsForSender connects to Nats for sending events
func ConnectNatsForSender(config *cacao_types_config.NatsConfig) (*NatsConnection, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-utilities.nats",
		"function": "ConnectNatsForSender",
	})

	logger.Tracef("trying to connect to %s", config.URL)

	options := []nats.Option{}

	if config.MaxReconnects >= 0 {
		options = append(options, nats.MaxReconnects(config.MaxReconnects))
	}

	if config.ReconnectWait >= 0 {
		reconnectWait := time.Duration(config.ReconnectWait) * time.Second
		options = append(options, nats.ReconnectWait(reconnectWait))
	}

	// Connect to Nats
	nc, err := nats.Connect(config.URL, options...)
	if err != nil {
		logger.WithError(err).Errorf("failed to connect to %s", config.URL)
		return nil, err
	}

	natsConn := &NatsConnection{
		Config:             config,
		Connection:         nc,
		EventHandlers:      map[cacao_types_service.QueryQueue]QueryEventHandler{},
		CloudEventHandlers: map[cacao_types_service.QueryQueue]QueryCloudEventHandler{},
	}

	logger.Tracef("established a connection to %s", config.URL)

	return natsConn, nil
}

// Disconnect disconnects Nats connection
func (conn *NatsConnection) Disconnect() error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-utilities.nats",
		"function": "NatsConnection.Disconnect",
	})

	logger.Tracef("trying to disconnect from %s", conn.Config.URL)

	if conn.Connection == nil {
		err := fmt.Errorf("connection is not established")
		logger.Error(err)
		return err
	}

	if conn.Connection.IsConnected() {
		conn.Connection.Close()
	}

	logger.Tracef("disconnected from %s", conn.Config.URL)

	// clear
	conn.Connection = nil
	conn.EventHandlers = map[cacao_types_service.QueryQueue]QueryEventHandler{}
	conn.CloudEventHandlers = map[cacao_types_service.QueryQueue]QueryCloudEventHandler{}
	return nil
}

// AddEventHandler adds a new event handler function to a specified subject. The subject must match the wildcard subject specified in NatsConfig.WildcardSubject
// eventHandler receives subject and JSON data of an event
func (conn *NatsConnection) AddEventHandler(subject cacao_types_service.QueryQueue, eventHandler QueryEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-utilities.nats",
		"function": "NatsConnection.AddEventHandler",
	})

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.EventHandlers[cacao_types_service.QueryQueue(subject)] = eventHandler

	logger.Tracef("added an event handler for a subject %s", subject)
	return nil
}

// AddCloudEventHandler adds a new event handler function to a specified subject. The subject must match the wildcard subject specified in NatsConfig.WildcardSubject
// eventHandler receives a cloudevent of an event
func (conn *NatsConnection) AddCloudEventHandler(subject cacao_types_service.QueryQueue, eventHandler QueryCloudEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-utilities.nats",
		"function": "NatsConnection.AddCloudEventHandler",
	})

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.CloudEventHandlers[cacao_types_service.QueryQueue(subject)] = eventHandler

	logger.Tracef("added an event handler for a subject %s", subject)
	return nil
}

// Request publishes Nats event
func (conn *NatsConnection) Request(subject cacao_types_service.QueryQueue, data interface{}) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-utilities.nats",
		"function": "NatsConnection.Request",
	})

	logger.Tracef("requesting a subject %s", subject)

	ce, err := cloudevent.CreateCloudEvent(data, string(subject), conn.Config.ClientID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event for subject %s", subject)
		return nil, err
	}

	eventJSON, err := ce.MarshalJSON()
	if err != nil {
		logger.WithError(err).Errorf("failed to marshal an event to JSON")
		return nil, err
	}

	requestTimeout := time.Duration(conn.Config.RequestTimeout) * time.Second
	responseMsg, err := conn.Connection.Request(ce.Type(), eventJSON, requestTimeout)
	if err != nil {
		logger.WithError(err).Errorf("failed to request a subject %s", ce.Type())
		return nil, err
	}

	logger.Tracef("requested a subject %s", ce.Type())

	return responseMsg.Data, nil
}

// RequestCloudEvent publishes Nats event
func (conn *NatsConnection) RequestCloudEvent(ce *cloudevents.Event) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-utilities.nats",
		"function": "NatsConnection.RequestCloudEvent",
	})

	// we put a subject into Event.Type instead of Event.Subject
	// please refer 'CreateCloudEvent' function in cloudevent.go
	logger.Tracef("publishing a subject %s", ce.Type())
	eventJSON, err := ce.MarshalJSON()
	if err != nil {
		logger.WithError(err).Errorf("failed to marshal an event to JSON")
		return nil, err
	}

	requestTimeout := time.Duration(conn.Config.RequestTimeout) * time.Second
	responseMsg, err := conn.Connection.Request(ce.Type(), eventJSON, requestTimeout)
	if err != nil {
		logger.WithError(err).Errorf("failed to request a subject %s", ce.Type())
		return nil, err
	}

	logger.Tracef("requested a subject %s", ce.Type())

	return responseMsg.Data, nil
}

// calls a event handler for an event and returns response
func (conn *NatsConnection) callEventHandler(event *cloudevents.Event) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-utilities.nats",
		"function": "NatsConnection.callEventHandler",
	})

	// we put a subject into Event.Type instead of Event.Subject
	// please refer 'CreateCloudEvent' function in cloudevent.go
	logger.Tracef("handling an event %s", event.Type())

	if handler, ok := conn.EventHandlers[cacao_types_service.QueryQueue(event.Type())]; ok {
		// has the handler for the event
		response, err := handler(cacao_types_service.QueryQueue(event.Type()), event.Data())
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return nil, err
		}
		return response, nil
	} else if handler, ok := conn.CloudEventHandlers[cacao_types_service.QueryQueue(event.Type())]; ok {
		// has the handler for the event
		response, err := handler(event)
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return nil, err
		}
		return response, nil
	} else {
		err := fmt.Errorf("failed to find an event handler for a type %s", event.Type())
		logger.Error(err)
		return nil, err
	}
}
