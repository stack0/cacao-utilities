// Package cloudevent contains utility functions for dealing with CloudEvents
package cloudevent

import (
	"encoding/json"
	"time"

	"github.com/rs/xid"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	nats "github.com/nats-io/nats.go"
	stan "github.com/nats-io/stan.go"
)

// CreateCloudEvent takes any object, eventType string, source string, and creates a resulting CloudEvent
// This utility provides the following conveniences:
// * uniformly assigns a new id of the format "cloudevent-" + xid
// * sets the time to UTC
// * generically marshals the data to json and appropriately assigns the cloudevent type
func CreateCloudEvent(data interface{}, eventType string, source string) (cloudevents.Event, error) {
	event := cloudevents.NewEvent()
	event.SetID("cloudevent-" + xid.New().String())
	event.SetType(eventType)
	event.SetTime(time.Now().UTC())
	event.SetSource(source)
	b, err := json.Marshal(data)
	event.SetData(cloudevents.ApplicationJSON, b)
	//event.SetData("application/json;charset=utf-8", b)

	return event, err
}

// ConvertNats converts NATS message to CloudEvents message
func ConvertNats(msg *nats.Msg) (cloudevents.Event, error) {
	event := cloudevents.NewEvent()
	err := json.Unmarshal(msg.Data, &event)
	return event, err
}

// ConvertStan converts STAN message to CloudEvents message
func ConvertStan(msg *stan.Msg) (cloudevents.Event, error) {
	event := cloudevents.NewEvent()
	err := json.Unmarshal(msg.Data, &event)
	return event, err
}
